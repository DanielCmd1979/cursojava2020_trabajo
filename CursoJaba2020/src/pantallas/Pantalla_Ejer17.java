package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JList;
import javax.swing.AbstractListModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;
import javax.swing.DropMode;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.JScrollPane;
import java.awt.Color;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;

public class Pantalla_Ejer17 {

	private JFrame frame;
	private String values[]=new String[10];
	private JComboBox cmbTabla;
	private JList list;
	private JLabel lblFila;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla_Ejer17 window = new Pantalla_Ejer17();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Pantalla_Ejer17() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 543, 511);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Tablas de multiplicar");
		lblNewLabel.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 30));
		lblNewLabel.setBounds(103, 33, 296, 39);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblElijaTabla = new JLabel("Elija tabla");
		lblElijaTabla.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 16));
		lblElijaTabla.setBounds(45, 110, 91, 21);
		frame.getContentPane().add(lblElijaTabla);
		
		String strListTabalas[] = new String [10];
		for(int i=0;i<10;i++){
			strListTabalas[i] = Integer.toString(i+1);
		}
		cmbTabla = new JComboBox();
		//se realiza un ciclo for para cargar el combo
		String  strTablas[] = new String[10]; //se define un array de strings
		for(int i=0;i<10;i++)
			strTablas[i]=Integer.toString(i+1);
			
		//se carga el combo con el array
		cmbTabla.setModel(new DefaultComboBoxModel(strTablas));
		
		cmbTabla.setBounds(165, 113, 109, 20);
		frame.getContentPane().add(cmbTabla);
		
		JButton btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//este es el texto cortado tal cual que reaccionara al boton
				int iTabla=Integer.parseInt((String) cmbTabla.getSelectedItem());
				int prod=0;
				for(int i=0;i<10;i++){
					prod=iTabla*i;
					values[i]= iTabla  + "x" + i + "=" + prod;
				}
				
				list.setModel(new AbstractListModel() {
					
					public int getSize() {
						return values.length;
					}
					public Object getElementAt(int index) {
						return values[index];
					}
				});
			
		};
		});
		btnCalcular.setBounds(341, 112, 89, 23);
		frame.getContentPane().add(btnCalcular);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(145, 186, 109, 191);
		frame.getContentPane().add(scrollPane);
		
		list = new JList();
		list.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				//tomo el valor seleccionado y los muestro en el label
				lblFila.setText(values[list.getSelectedIndex()]);
				
			}
		});
		
		list.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 16));

		scrollPane.setViewportView(list);
		
		lblFila = new JLabel("");
		lblFila.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 24));
		lblFila.setBackground(Color.GREEN);
		lblFila.setOpaque(true);
		lblFila.setBounds(375, 227, 100, 39);
		frame.getContentPane().add(lblFila);
		
	}
}
 