package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTable;
import java.awt.BorderLayout;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.ComponentOrientation;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Pantalla_Ejer18_grilla2 {

	private JFrame frame;
	private JTable table;
	private JComboBox comboBoxDesde;
	private JComboBox comboBoxHasta;
	private String strTitulos [];//= new String [10];
	private String strTablas [][];//=new String [10][10];

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla_Ejer18_grilla2 window = new Pantalla_Ejer18_grilla2();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Pantalla_Ejer18_grilla2() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 596, 396);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 31, 406, 220);
		frame.getContentPane().add(scrollPane);
		
		table = new JTable();
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
	
		

		scrollPane.setViewportView(table);
		
		JButton btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int iTablaDesde=Integer.parseInt((String)comboBoxDesde.getSelectedItem());
				int iTablaHasta=Integer.parseInt((String)comboBoxHasta.getSelectedItem());
				int iCantColumna = iTablaHasta - iTablaDesde + 1;
				
				strTitulos = new String [iCantColumna];
				strTablas = new String[iTablaDesde][iTablaHasta];
				for(int col=0;col<10;col++){
					strTitulos[col]="tabla del " + col;
					for(int fila=0;fila<10;fila++)
						strTablas[fila][col]=col + "x" + fila + "="+ (fila*col);
					

				}
				
				table.setModel(new DefaultTableModel(strTablas,strTitulos
					));
			}
		});
		btnCalcular.setBounds(481, 324, 89, 23);
		frame.getContentPane().add(btnCalcular);
		
		comboBoxDesde = new JComboBox();
		comboBoxDesde.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"}));
		comboBoxDesde.setBounds(481, 55, 65, 20);
		frame.getContentPane().add(comboBoxDesde);
		
		comboBoxHasta = new JComboBox();
		comboBoxHasta.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"}));
		comboBoxHasta.setBounds(481, 127, 65, 20);
		frame.getContentPane().add(comboBoxHasta);
	}
}
