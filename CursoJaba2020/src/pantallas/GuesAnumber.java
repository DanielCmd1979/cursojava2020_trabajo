package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class GuesAnumber {

	private JFrame frame;
	private JTextField textNumeroElegido;
	private JLabel lblArribaAbajo;
	private JLabel lblElegido;
	Integer cantidadIntentos=0;
	private JLabel lblTextoArribaAbajo;
	private JLabel lblCantIntentos;
	private JButton btnLimpiar;
	private JComboBox cmbNumeroElegido;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GuesAnumber window = new GuesAnumber();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GuesAnumber() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblElijeUnNumero = new JLabel("Elije un numero");
		lblElijeUnNumero.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblElijeUnNumero.setBounds(174, 11, 108, 17);
		frame.getContentPane().add(lblElijeUnNumero);
		
		JLabel lblElijeUnNumero_1 = new JLabel("Elije un numero entre 1 y 99");
		lblElijeUnNumero_1.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblElijeUnNumero_1.setHorizontalAlignment(SwingConstants.LEFT);
		lblElijeUnNumero_1.setBounds(10, 123, 175, 15);
		frame.getContentPane().add(lblElijeUnNumero_1);
		
		textNumeroElegido = new JTextField();
		textNumeroElegido.setVisible(false);
		textNumeroElegido.setText("0");
		textNumeroElegido.setHorizontalAlignment(SwingConstants.RIGHT);
		textNumeroElegido.setBounds(211, 121, 86, 20);
		frame.getContentPane().add(textNumeroElegido);
		textNumeroElegido.setColumns(10);
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//int imiValor = Integer.parseInt(textNumeroElegido.getText());
				int imiValor = Integer.parseInt((String)cmbNumeroElegido.getSelectedItem());
				int ivalorCompu = Integer.parseInt(lblElegido.getText());
				
				if(imiValor>ivalorCompu)
				{

					lblTextoArribaAbajo.setVisible(true);
					lblTextoArribaAbajo.setText("Mas abajo");
					
					lblArribaAbajo.setVisible(true);
					lblArribaAbajo.setIcon(new ImageIcon(GuesAnumber.class.getResource("/iconos/dedoAbajo_32px.png")));					
				}
				else if(imiValor<ivalorCompu)
				{
					
					lblTextoArribaAbajo.setVisible(true);
					lblTextoArribaAbajo.setText("Mas arriba");
					
					lblArribaAbajo.setVisible(true);
					lblArribaAbajo.setIcon(new ImageIcon(GuesAnumber.class.getResource("/iconos/dedoArriba_32px.png")));		
				}
				else
				{
					
					lblTextoArribaAbajo.setVisible(true);
					lblTextoArribaAbajo.setText("Le pegaste!!! ");
					
					lblElegido.setVisible(true);
					
					lblArribaAbajo.setVisible(true);
					lblArribaAbajo.setIcon(new ImageIcon(GuesAnumber.class.getResource("/iconos/Gano_32px.png")));	
				}
				cantidadIntentos++;
				lblCantIntentos.setText(Integer.toString(cantidadIntentos));
				
			}
		});
		btnAceptar.setBounds(318, 137, 89, 23);
		frame.getContentPane().add(btnAceptar);
		
		lblArribaAbajo = new JLabel("");
		lblArribaAbajo.setVisible(false);
		lblArribaAbajo.setIcon(new ImageIcon(GuesAnumber.class.getResource("/iconos/dedoArriba_32px.png")));
		lblArribaAbajo.setBounds(139, 166, 71, 60);
		frame.getContentPane().add(lblArribaAbajo);
		
		lblTextoArribaAbajo = new JLabel("");
		lblTextoArribaAbajo.setVisible(false);
		lblTextoArribaAbajo.setBounds(42, 191, 87, 17);
		frame.getContentPane().add(lblTextoArribaAbajo);
		
		lblElegido = new JLabel("");
		lblElegido.setVisible(false);
		lblElegido.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblElegido.setHorizontalAlignment(SwingConstants.CENTER);
		lblElegido.setBackground(Color.GREEN);
		lblElegido.setOpaque(true);
		lblElegido.setBounds(322, 53, 46, 32);
		frame.getContentPane().add(lblElegido);
		
		int iNumElegido = (int)(Math.random()*1000%99)+1;
		lblElegido.setText(Integer.toString(iNumElegido));
		
		JLabel lblElElegidoEs = new JLabel("El elegido es...");
		lblElElegidoEs.setBounds(241, 74, 71, 14);
		frame.getContentPane().add(lblElElegidoEs);
		
		JLabel lblCantidadDeIntentos = new JLabel("Cantidad de intentos");
		lblCantidadDeIntentos.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblCantidadDeIntentos.setBounds(10, 74, 130, 15);
		frame.getContentPane().add(lblCantidadDeIntentos);
		
		lblCantIntentos = new JLabel("");
		lblCantIntentos.setHorizontalAlignment(SwingConstants.RIGHT);
		lblCantIntentos.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblCantIntentos.setBounds(150, 65, 60, 23);
		frame.getContentPane().add(lblCantIntentos);
		
		btnLimpiar = new JButton("Limpiar");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblElegido.setText("");
				textNumeroElegido.setText("");
				lblTextoArribaAbajo.setText("");
				lblCantIntentos.setText("");
				cmbNumeroElegido = new JComboBox();
				String strList[]= new String[99];//defino un array
				for(int i =0; i<99; i++) 
				
					strList[i]=Integer.toString(i+1);
				cmbNumeroElegido.setModel(new DefaultComboBoxModel(strList));
				
			}
		});
		btnLimpiar.setBounds(318, 228, 89, 23);
		frame.getContentPane().add(btnLimpiar);
		
		cmbNumeroElegido = new JComboBox();
		String strList[]= new String[99];//defino un array
		for(int i =0; i<99; i++) 
		
			strList[i]=Integer.toString(i+1);
		
		//cmbNumeroElegido.setModel(new DefaultComboBoxModel(new String[] {"Elegir...", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60", "61", "62", "63", "64", "65", "66", "67", "68", "69", "70", "71", "72", "73", "74", "75", "76", "77", "78", "79", "80", "81", "82", "83", "84", "85", "86", "87", "88", "89", "90", "91", "92", "93", "94", "95", "96", "97", "98", "99"}));
		cmbNumeroElegido.setModel(new DefaultComboBoxModel(strList));
		cmbNumeroElegido.setBounds(211, 138, 86, 20);
		frame.getContentPane().add(cmbNumeroElegido);
	}
}
