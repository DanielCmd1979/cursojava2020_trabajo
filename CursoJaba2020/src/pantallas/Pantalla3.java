package pantallas;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import java.awt.SystemColor;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Pantalla3 {

	private JFrame frame;
	private JTextField textFieldNota1;
	private JTextField textFieldNota2;
	private JTextField textFieldNota3;
	private JLabel lblImagen;
	private JLabel lblResult;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla3 window = new Pantalla3();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Pantalla3() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblPromedio = new JLabel("Promedio de notas");
		lblPromedio.setHorizontalAlignment(SwingConstants.CENTER);
		lblPromedio.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblPromedio.setBounds(165, 11, 130, 17);
		frame.getContentPane().add(lblPromedio);
		
		JLabel lblNota1 = new JLabel("Nota 1");
		lblNota1.setBounds(39, 75, 46, 14);
		frame.getContentPane().add(lblNota1);
		
		JLabel lblNota2 = new JLabel("Nota 2");
		lblNota2.setBounds(39, 125, 46, 14);
		frame.getContentPane().add(lblNota2);
		
		JLabel lblNota3 = new JLabel("Nota 3");
		lblNota3.setBounds(39, 174, 46, 14);
		frame.getContentPane().add(lblNota3);
		
		textFieldNota1 = new JTextField();
		textFieldNota1.setText("0");
		textFieldNota1.setHorizontalAlignment(SwingConstants.RIGHT);
		textFieldNota1.setBounds(108, 72, 86, 20);
		frame.getContentPane().add(textFieldNota1);
		textFieldNota1.setColumns(10);
		
		textFieldNota2 = new JTextField();
		textFieldNota2.setText("0");
		textFieldNota2.setHorizontalAlignment(SwingConstants.RIGHT);
		textFieldNota2.setBounds(108, 122, 86, 20);
		frame.getContentPane().add(textFieldNota2);
		textFieldNota2.setColumns(10);
		
		textFieldNota3 = new JTextField();
		textFieldNota3.setText("0");
		textFieldNota3.setHorizontalAlignment(SwingConstants.RIGHT);
		textFieldNota3.setBounds(108, 171, 86, 20);
		frame.getContentPane().add(textFieldNota3);
		textFieldNota3.setColumns(10);
		
		lblResult = new JLabel("");
		lblResult.setHorizontalAlignment(SwingConstants.RIGHT);
		lblResult.setBackground(SystemColor.activeCaption);
		lblResult.setOpaque(true);
		lblResult.setBounds(257, 112, 125, 27);
		frame.getContentPane().add(lblResult);
		
		JButton btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Obtengo los 3 valores de las notas
				float fnota1=Float.parseFloat(textFieldNota1.getText());
				float fnota2=Float.parseFloat(textFieldNota2.getText());
				float fnota3=Float.parseFloat(textFieldNota3.getText());
				
				float fpromedio=(fnota1+fnota2+fnota3)/3;
				
				
				if(fpromedio >= 7)
				{
					lblResult.setText(Float.toString(fpromedio));
					lblResult.setBackground(Color.GREEN);
					lblImagen.setIcon(new ImageIcon(Pantalla3.class.getResource("/iconos/dedoArriba_32px.png")));
				}
				else
				{
					lblResult.setText(Float.toString(fpromedio));
					lblResult.setBackground(Color.RED);
					lblImagen.setIcon(new ImageIcon(Pantalla3.class.getResource("/iconos/dedoAbajo_32px.png")));
				}
				
				
			}
		});
		btnCalcular.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnCalcular.setBounds(108, 216, 89, 23);
		frame.getContentPane().add(btnCalcular);
		
		JButton btnLimpiar = new JButton("Limpiar");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textFieldNota1.setText("0");
				textFieldNota2.setText("0");
				textFieldNota3.setText("0");
				lblResult.setText("");
				lblResult.setBackground(SystemColor.activeCaption);
				lblImagen.setIcon(new ImageIcon(Pantalla3.class.getResource("/iconos/papel_32px.png")));
			}
		});
		btnLimpiar.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnLimpiar.setBounds(257, 216, 89, 23);
		frame.getContentPane().add(btnLimpiar);
		
		lblImagen = new JLabel("");
		lblImagen.setIcon(new ImageIcon(Pantalla3.class.getResource("/iconos/papel_32px.png")));
		lblImagen.setBounds(257, 39, 46, 62);
		frame.getContentPane().add(lblImagen);
	}

}
