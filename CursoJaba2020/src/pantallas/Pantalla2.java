package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;

public class Pantalla2 {

	private JFrame frame;
	private JTextField textFieldBmin;
	private JTextField textFieldBmax;
	private JTextField textFieldSmin;
	private JTextField textFieldSmax;
	private JTextField textFieldImin;
	private JTextField textFieldImax;
	private JTextField textFieldLmin;
	private JTextField textFieldLmax;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla2 window = new Pantalla2();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Pantalla2() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 334);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblSumarNmeros = new JLabel("Tipo de datos");
		lblSumarNmeros.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblSumarNmeros.setBounds(157, 11, 116, 29);
		frame.getContentPane().add(lblSumarNmeros);
		
		textFieldBmin = new JTextField();
		textFieldBmin.setHorizontalAlignment(SwingConstants.RIGHT);
		textFieldBmin.setBounds(89, 75, 150, 20);
		frame.getContentPane().add(textFieldBmin);
		textFieldBmin.setColumns(10);
		
		textFieldBmax = new JTextField();
		textFieldBmax.setHorizontalAlignment(SwingConstants.RIGHT);
		textFieldBmax.setBounds(245, 75, 150, 20);
		frame.getContentPane().add(textFieldBmax);
		textFieldBmax.setColumns(10);
		
		textFieldSmin = new JTextField();
		textFieldSmin.setHorizontalAlignment(SwingConstants.RIGHT);
		textFieldSmin.setBounds(89, 115, 150, 20);
		frame.getContentPane().add(textFieldSmin);
		textFieldSmin.setColumns(10);
		
		JLabel lblByte = new JLabel("Byte");
		lblByte.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblByte.setBounds(23, 75, 86, 19);
		frame.getContentPane().add(lblByte);
		
		JLabel lblShort = new JLabel("Short");
		lblShort.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblShort.setBounds(23, 114, 46, 19);
		frame.getContentPane().add(lblShort);
		
		JLabel lblInteger = new JLabel("Integer");
		lblInteger.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblInteger.setBounds(23, 146, 71, 29);
		frame.getContentPane().add(lblInteger);
		
		JLabel lblLong = new JLabel("Long");
		lblLong.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblLong.setBounds(23, 186, 46, 20);
		frame.getContentPane().add(lblLong);
		
		textFieldSmax = new JTextField();
		textFieldSmax.setHorizontalAlignment(SwingConstants.RIGHT);
		textFieldSmax.setBounds(245, 114, 150, 20);
		frame.getContentPane().add(textFieldSmax);
		textFieldSmax.setColumns(10);
		
		textFieldImin = new JTextField();
		textFieldImin.setHorizontalAlignment(SwingConstants.RIGHT);
		textFieldImin.setText("");
		textFieldImin.setBounds(89, 152, 150, 20);
		frame.getContentPane().add(textFieldImin);
		textFieldImin.setColumns(10);
		
		textFieldImax = new JTextField();
		textFieldImax.setHorizontalAlignment(SwingConstants.RIGHT);
		textFieldImax.setBounds(245, 151, 150, 20);
		frame.getContentPane().add(textFieldImax);
		textFieldImax.setColumns(10);
		
		textFieldLmin = new JTextField();
		textFieldLmin.setHorizontalAlignment(SwingConstants.RIGHT);
		textFieldLmin.setText("");
		textFieldLmin.setBounds(89, 188, 150, 20);
		frame.getContentPane().add(textFieldLmin);
		textFieldLmin.setColumns(10);
		
		textFieldLmax = new JTextField();
		textFieldLmax.setHorizontalAlignment(SwingConstants.RIGHT);
		textFieldLmax.setBounds(245, 187, 150, 20);
		frame.getContentPane().add(textFieldLmax);
		textFieldLmax.setColumns(10);
		
		JButton btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				byte bmin=Byte.MIN_VALUE;
				byte bmax=Byte.MAX_VALUE;
				
				textFieldBmin.setText(Byte.toString(bmin));
				textFieldBmax.setText(Byte.toString(bmax));
				
				short smin=Short.MIN_VALUE;
				short smax=Short.MAX_VALUE;
				
				textFieldSmin.setText(Short.toString(smin));
				textFieldSmax.setText(Short.toString(smax));
				
				int imin=Integer.MIN_VALUE;
				int imax=Integer.MAX_VALUE;
				
				textFieldImin.setText(Integer.toString(imin));
				textFieldImax.setText(Integer.toString(imax));
				
				long lmin=Long.MIN_VALUE;
				long lmax=Long.MAX_VALUE;
				
				textFieldLmin.setText(Long.toString(lmin));
				textFieldLmax.setText(Long.toString(lmax));
			}
		});
		btnCalcular.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnCalcular.setBounds(184, 248, 89, 23);
		frame.getContentPane().add(btnCalcular);
	}
}
